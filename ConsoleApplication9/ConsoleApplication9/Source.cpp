#include <iostream>
#include<ctime>
#include<vector>
#include <algorithm> 

using namespace std;
vector <int> IN;
vector <int> OUT;

bool moveMin(vector<int> &in, vector<int> & out)
{

	for (int i = 0; i < IN.size(); i++){
		for (int j = i; j < IN.size(); j++)   {
			if (IN[i]>IN[j]){ int temp = IN[i]; IN[i] = IN[j]; IN[j] = temp; }
		}
	}
	cout << "Sorted Array" << endl;

	for (int i = 0; i < IN.size(); i++)
	{
		cout << IN[i] << " ";
	}

	cout << endl;

	if (equal(IN.begin(), IN.begin(), OUT.begin()))
	{ return true; }
	else return false;
}
void  testMoveMin()
{
	srand(time(NULL));

	for (int i = 0; i < 7; i++)
	{
		IN.push_back((rand() % 100) + 1);
	} //Storing random no. in vector

	cout << "Original Array" << endl;

	for (int i = 0; i < 7; i++)
	{
		cout << IN[i] << " ";
	}//Printing number stored in vector
	cout << endl;

	sort(IN.begin(), IN.end()); //sorting elements

	cout << endl << "Sorted vector" << endl;
	for (int i = 0; i < 7; i++)
	{
		cout << IN[i] << " ";
	}  //Printing sorted elements
	cout << endl << endl;

	cout << "Adding a random number to the vector" << endl << endl;
	(void)IN.insert(IN.end(), (rand() % *max_element(IN.begin(), IN.end())));  //Adding another randon number

	cout << "Printing IN elemements";
	cout << endl;
	for (int i = 0; i < 8; i++){ cout << IN[i] << " "; }//Printing new unsorted vector
	cout << endl;

	IN.begin();
	for (int i = 0; i < 8; i++){ OUT.push_back(IN[i]); } //Copying IN elements to OUT
	cout << endl;

	cout << "Printing OUT elements" << endl;
	for (int i = 0; i < 8; i++){ cout << OUT[i] << " "; } //Printing OUT vector
	cout << endl;

	cout << "\nPrinting SORTED OUT elements \n";
	sort(OUT.begin(), OUT.end());
	for (int i = 0; i < 8; i++){ cout << OUT[i] << " "; } //Printing sorted OUT
	cout << endl << endl;

	bool result = moveMin(IN, OUT);

	cout << endl;

	cout << "Are the both vectors equal?" << endl << result << endl << endl;

}
int main()
{
	testMoveMin();
}