/**
*@title lab1
*@author KHUZEMAH SAEED	 (ksaeed.bscs15seecs@seecs.edu.pk)
*@version 1.0
*/

#include<iostream>

using namespace std;

/**
* This function divide two numbers given by user
*
*
* @param[in] number 1
* @param[out] number 2
* @return sum of number 1 and 2
*/

void Devision(double x, double y)
{
	cout << "Result of div : " << x / y << endl;
}
/**
* This function mul two numbers given by user
*
*
* @param[in] number 1
* @param[out] number 2
* @return sum of number 1 and 2
*/

void multiplication(double x, double y)
{
	cout << "Result of Multi: " << x*y << endl;
}
/**
* This function adds two numbers given by user
*
*
* @param[in] number 1
* @param[out] number 2
* @return sum of number 1 and 2
*/
void Addition(double x, double y)
{
	cout << "Result of add : " << x + y << endl;
}
/**
* This function sub two numbers given by user
*
*
* @param[in] number 1
* @param[out] number 2
* @return sum of number 1 and 2
*/
void subtraction(double x, double y)
{
	cout << "Result  of sub: " << x - y << endl;
}
/**
* This is the main function of the program
*
*
* @param[in] number 1
* @param[out] number 2
* @return sum of number 1 and 2
*/

int main()
{
	double a, b;

	cout << "Enter first Number :" << endl;
	cin >> a;
	cout << "Enter second Number :" << endl;
	cin >> b;
	multiplication(a, b);
	Devision(a, b);
	Addition(a, b);
	subtraction(a, b);
}
